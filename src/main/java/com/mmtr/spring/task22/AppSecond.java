package com.mmtr.spring.task22;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppSecond {
    public static void main(String[] args) {
        new AnnotationConfigApplicationContext("com.mmtr.spring.task22.configuration");
    }
}
