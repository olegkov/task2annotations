package com.mmtr.spring.task22.configuration;

import com.mmtr.spring.task22.component.Dictionary;
import com.mmtr.spring.task22.service.MyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@ComponentScan()
public class MyConfig {
    @Bean(initMethod = "init")
    public Dictionary dictionary1(){
        File dictionary = new File("dictionary1.dat");
        return new Dictionary(dictionary, "^(?<key>[a-zA-z]{4})$");
    }

    @Bean(initMethod = "init")
    public Dictionary dictionary2(){
        File dictionary = new File("dictionary2.dat");
        return new Dictionary(dictionary, "^(?<key>\\d{5})$");
    }

    @Bean
    public List<Dictionary> dictionaries(){
        List<Dictionary> dictionaries = new ArrayList<>();
        dictionaries.add(dictionary1());
        dictionaries.add(dictionary2());
        return Collections.unmodifiableList(dictionaries);
    }

    @Bean(initMethod = "showMenu")
    public MyService service(){
        return new MyService();
    }
}
